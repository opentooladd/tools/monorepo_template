# Context variables
####################

# TODO : review this constraint
# solve some regex questions in `guard-%:`
SHELL := /bin/bash
.ONESHELL:
.SHELLFLAGS = -c -e

# Useful to ensure files produced in volumes over docker-compose exec
# commands are not "root privileged" files.
export HOST_UID=$(shell id -u)
export HOST_GID=$(shell id -g)
export HOST_ID=${HOST_UID}:${HOST_GID}


PROJECT_ROOT_DIR := $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))

# PROJECT_NAME defaults to name of the current directory.
# should not be changed if you follow GitOps operating procedures.
export PROJECT_NAME = $(notdir $(PROJECT_ROOT_DIR))

# export CURRENT_DATE = $(shell date +"%Y%m%d")

export INFRA_BASE_PATH = $(PROJECT_ROOT_DIR)/infra
export INFRA_DOCKER_PATH = $(PROJECT_ROOT_DIR)/infra/docker
# export SRC_BASE_PATH = $(PROJECT_ROOT_DIR)/src
# export SCRIPT_PATH = $(PROJECT_ROOT_DIR)/scripts
# export DOMAIN_BASE_PATH = $(PROJECT_ROOT_DIR)/domain

export ENV_ROOT_PATH = $(PROJECT_ROOT_DIR)/env
ifneq (,$(wildcard ${ENV_ROOT_PATH}/project.env))
    include ${ENV_ROOT_PATH}/project.env
    export
endif
export ENV_BASE_PATH = ${ENV_ROOT_PATH}/${DEPLOY_ENV}
export ENV_BACKUP_ROOT_PATH = ${ENV_ROOT_PATH}/backup
export ENV_BACKUP_BASE_PATH = ${ENV_BACKUP_ROOT_PATH}/${DEPLOY_ENV}
export ENV_CONFIGURATION_PATH = ${ENV_BASE_PATH}/configuration

# Include services *.env files

# ifneq (,$(wildcard ${ENV_CONFIGURATION_PATH}/networks.env))
#     include ${ENV_CONFIGURATION_PATH}/networks.env
#     export
# endif

# ifneq (,$(wildcard ${ENV_CONFIGURATION_PATH}/proxy.env))
#     include ${ENV_CONFIGURATION_PATH}/proxy.env
#     export
# endif

# ifneq (,$(wildcard ${ENV_CONFIGURATION_PATH}/smtp.env))
#     include ${ENV_CONFIGURATION_PATH}/smtp.env
#     export
# endif

# export VCS_USER = $(shell git config user.name)
# export VCS_EMAIL = $(shell git config user.email)


# Docker environment setup
export DOCKER_NETWORK = $(PROJECT_NAME)/network


## Meta commands used for internal purpose
############################################

# This guard meta command allows to check if environment variable is defined 
guard-%:
	@if [ -z '${${*}}' ]; then echo 'ERROR: variable $* not defined' && exit 1; fi
	@if [[ '${${*}}' =~ ToBeSet ]]; then \
		echo "ERROR: variable $* not set, please change its value (\"${${*}}\" is not a valid value)" && \
		exit 1; \
	fi


## Infra
#########

infra-init: services-config-all infra-run services-init-all
# 	make services-reload-all

# Doesn't run services, infra setup only
infra-run: guard-DOCKER_COMPOSE
	${DOCKER_COMPOSE} up -d --remove-orphans
# 	${DOCKER_COMPOSE} up -d --remove-orphans --build

infra-watch: infra-run logs

# stop containers and processes
infra-stop: guard-DOCKER_COMPOSE 
	${DOCKER_COMPOSE} stop 

infra-restart: infra-stop infra-run


## Host environment
####################

# we dissociate 'host' from 'infra' in command names to avoid deployment "mistakes".
# host is reserved for "risky" operations even if some actions could be discussed as infra.

host-guard: guard-DEPLOY_ENV guard-DOCKER_COMPOSE

# This command :
# 	- prepares "env files" and services configuration structures according template.
host-env-prepare: host-guard
	mkdir -p ${ENV_CONFIGURATION_PATH}
	cp -r ${INFRA_BASE_PATH}/configuration/* ${ENV_CONFIGURATION_PATH}

# Remove environment variable names from services configuration files to apply their values
# TODO : make envsubst quiet/silent
host-apply-config: host-guard
	@echo "Nothing to do"
# 	find ${ENV_CONFIGURATION_PATH} -type f | xargs -I {} sh -c "envsubst < {} | tee {}"

# This command moves current environment configuration to a backup directory. 
# TODO : should we allow silent backup remove ?
host-env-backup: host-guard
	-[ -d ${ENV_BASE_PATH} ] && { rm -rf ${ENV_BACKUP_BASE_PATH} && mkdir -p ${ENV_BACKUP_BASE_PATH}; }
	-mv ${ENV_BASE_PATH}/* ${ENV_BACKUP_BASE_PATH}
	rm -rf ${ENV_BASE_PATH}

# This command restores current environment configuration.
host-env-restore: host-guard
	rm -rf ${ENV_BASE_PATH}
	mkdir ${ENV_BASE_PATH}
	- cp -r ${ENV_BACKUP_BASE_PATH}/* ${ENV_BASE_PATH}

# Reset environment to fresh configured project 
# WARNING : remove all docker objects (even other projects one)
host-clean: host-guard clean-all host-docker-clean host-env-restore

# host-reset: host-clean

# Complete docker environment purge
# WARNING : This command will purge all docker environment (all projects)
# TODO : Look for a "softer" reset solution 
# 	https://github.com/docker/compose/issues/6159#issuecomment-862999377
#	https://docs.docker.com/engine/reference/commandline/compose/#use--p-to-specify-a-project-name
host-docker-clean:
	- docker stop $(shell docker ps -a -q)
	- docker rm -v $(shell docker ps -a -q)
	- docker volume rm $(shell docker volume ls -q)
	- docker network rm $(shell docker network ls -q --filter type=custom)
	- docker rmi $(shell docker images -a -q) -f
	- docker builder prune -f
	- docker system prune -a -f

# WARN this command should be used during dev only cause it prints some credentials
host-log:
	@printf '\n ==== Makefile ENV  ==== \n\n'
	@printenv | sort


## Diagnostic
##############

logs: guard-DOCKER_COMPOSE
	$(DOCKER_COMPOSE) ps
	$(DOCKER_COMPOSE) logs -f --tail=100

# log-proxy-env: guard-DOCKER_COMPOSE
# 	${DOCKER_COMPOSE} run proxy.nginx printenv | sort
# 	${DOCKER_COMPOSE} run proxy.letsencrypt printenv | sort

log-system:
	printenv
	docker info
	df -h
	docker system df
# 	docker stats

# TODO : add an history graph view
# git log --pretty=format:"%h %s" --graph --since=1.weeks

# log-SERVICE.db: guard-DOCKER_COMPOSE
# 	$(DOCKER_COMPOSE) logs -f --tail=100 SERVICE.db

# log-SERVICE.db-env: guard-DOCKER_COMPOSE
# 	${DOCKER_COMPOSE} run SERVICE.db printenv | sort

# log-SERVICE.api: guard-DOCKER_COMPOSE
# 	$(DOCKER_COMPOSE) logs -f --tail=100 SERVICE.api


## Clean
#########

clean-config: host-env-backup
	rm -rf ${ENV_CONFIGURATION_PATH}

clean-all: services-clean-all clean-config


## Shell
#########

# shell-SERVICE.app: guard-DOCKER_COMPOSE
# 	@${DOCKER_COMPOSE} run SERVICE.app sh -c '/bin/sh'

# shell-SERVICE.postgres: guard-DOCKER_COMPOSE
# 	@${DOCKER_COMPOSE} exec -e PGPASSWORD=${SERVICE_DB_PASS_API_ADMIN} SERVICE.db psql -U ${SERVICE_DB_USER_API_ADMIN} -d ${SERVICE_DB_NAME}

# shell-proxy: guard-DOCKER_COMPOSE
# 	${DOCKER_COMPOSE} run -u root:root proxy.nginx sh -c '/bin/sh'

# shell-proxy.letsencrypt: guard-DOCKER_COMPOSE
# 	${DOCKER_COMPOSE} run -u root:root proxy.letsencrypt sh -c '/bin/sh'

# shell-smtp.maildev: guard-DOCKER_COMPOSE
# 	${DOCKER_COMPOSE} run -u root:root smtp.maildev sh -c '/bin/sh'


## Tests
#########

# test-all: test-phpunit test-sonarqube

# test-SERVICE: guard-DOCKER_COMPOSE
# 	${DOCKER_COMPOSE} run SERVICE.CONTAINER sh -c 'echo "TODO"'


## Services admin
##################

services-reload-all: infra-stop infra-run

services-init-all:
# 	echo "todo"

services-run-all:
# 	echo "todo"

services-config-all:
# services-config-all: proxy-config

# services-backup-all:
# 	echo "todo"

# services-restore-all:
# 	echo "todo"

services-clean-all:
# 	echo "todo"

## SERVICE.app 
###############

# SERVICE-guard: guard-XXXX guard-YYYY

# SERVICE.app-clean: SERVICE-guard

# SERVICE.app-init: SERVICE-guard

# SERVICE.app-run: SERVICE-guard

# SERVICE.app-watch: SERVICE-guard



## Proxy service 
#################

# proxy-guard: guard-PROXY_DEFAULT_EMAIL guard-PROXY_BASE_HOSTNAME

# proxy-config:
# 	@sh -c '${SCRIPT_PATH}/proxy_configure.sh ${ENV_CONFIGURATION_PATH}/proxy/etc/nginx/conf.d ${PROXY_BASE_HOSTNAME_LIST}'

