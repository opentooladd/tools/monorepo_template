# Project configuration

This documentation describes basic principles of this project configuration.

Configuration is managed at three different levels :
1. ***Local configuration*** level is defined for Project itself.
    
    This configuration is managed by `env/project.env` file and you can find a precise description of this file in `env/project.env.template`.
2. ***Environment configuration*** level is defined for `$DEPLOY_ENV` environment.
    
    This configuration is defined by `env/{DEPLOY_ENV}/configuration/*.env` files.
    > :point_up: *Environment configuration* can depend on *Local configuration*.
3. ***Services configuration*** level is defined for `$DEPLOY_ENV` environment for each service managed in this project.

    This configuration is defined by `env/{DEPLOY_ENV}/configuration/{service_tag}/` directories. You can find description in `infra/configuration/{service_tag}/` directories.
    > :point_up: *Services configuration* depends on *Environment configuration*.


## Configuration conventions

**TODO** : review this part with last updates.

## Configuration process

> :point_up: In order to understand configuration usage, you can take a look at Makefile commands.

For install, please follow this process :
1. *Local configuration*
   1. `cp env/project.env.template env/project.env`
   2. Edit `project.env` values.
2. *Environment configuration*
   1. `make host-env-prepare`
   2. Edit all `*.env` values you need.
3. *services configuration*
   1. `make host-apply-config`


## Using environment variables

If you have *.env files, variables from env files are used.

If files not defined, variables are defined from environment.

If you don't have .env files neiher environment variables, you should encounter some weird behaviour.

