# Monorepo template 

This repository offers a monorepo template.

It should help building an efficient environment for dev/devops development (prototyping, testing, etc.)

This structure result from a long monorepo experience and is in continuous evolution.
Feel free to share any ideas / suggestions to enhance it.


## Prerequisites

This project is made to limit system dependencies for any development/home made deployment project.

Basic dependencies are :
- Makefile
  > [Learn Makefiles](https://makefiletutorial.com/))
- Docker engine & Docker compose
  > [Docker docs](https://docs.docker.com/)
- basic unix system commands (like `rm`, `cd`, `shell`, ...).
  > :point_up: See root `Makefile` to find used commands


## Usage

### Initialization

- Copy and rename the project (reset `.git` if necessary)
- Define your project
  - [ ] Define licencing & change root `LICENCE.md` file
  - [ ] Update `Readme.md`
    - remove `# Monorepo template` section and its subsections
    - update `# <Project>` section
- Update documentation
  - [ ] Check / validate install process
  - [ ] Add specific project documentation / descriptions
- Architecture
  - [ ] Define target architecture and testing strategy
  - [ ] Review docker networking
  - [ ] Update architecture schema in documentation (`doc/architecture.drawio`)

### Template "extensions"

Some extensions have been considered but not added to default template structure :
- A `domain/` directory can be added at project root for specific data storage and data structure descriptions/scripts
- A `src/` directory can be added at project root for specific application sources (source code, git sub module repositories, etc.)
- A `script/` directory can be added at project root for specific infra / deployment scripts

Adding those directories may induce some Makefile / infra adaptations (dedicated environment variables, etc.).

### Add your services

You have to understand monorepo structure and usage for specific use case (see `./doc/configuration/*`).

1. Add your service / code to global project infra. You may 
   * add a dedicated `infra/docker/<SERVICE>.docker-compose.yaml`

   And / Or

   * Create dedicated `infra/docker/<SERVICE>/Dockerfile<.SUB-IMAGE>` 
2. [If necessary] add a `infra/configuration/<SERVICE>.env` as configuration "env file" template (add comments)
3. [If necessary] create `infra/configuration/SERVICE` directory and add service specific configuration files.
4. Update `env/deploy.env` and `Makefile` files to add your service

> :information_source: You may use "proxy service" as a basic example.
> - docker-compose file already exists for service.
> - For this specific service you may update some docker-compose file for other services
> - Update your local `/etc/hosts` file and edit your `env/configuration/{$DEPLOY_ENV}/proxy.env` file. (If it doesn't exist, you may review project install process)
> - Update your Makefile (uncomment `${ENV_CONFIGURATION_PATH}/proxy.env` lines)
> - Then restarting the project should show that your well configured services are accessible from your new custom local hostname.


## Monorepo template perspectives and ideas

- Using conventions and structure to create a dedicated cross platform cli tool.
- Developping a recursive structure to enhance modularity.
- Review structure for a more efficient CI/CD integration (Infrastructure As Code; integration with Vault, Terraform, etc. ?)
- Review network usage and standards
- Review enhance `Makefile` structure and usage.
- Docker to Podman (?)


# <Project>

**TODO : describe your project** 

## First install

### Configure the project

> cf. `{repos}/doc/configuration/project_configuration.md`

> :warning: If you consider using "proxy" service for specific local hostname usage, please think to add all your services url (SERVICE.{PROXY_BASE_HOSTNAME}) to your `hosts` file. \
> See [How to Edit Hosts File in Linux, Windows, or Mac](https://phoenixnap.com/kb/how-to-edit-hosts-file-in-windows-mac-or-linux)

1. *Project configuration*
   - `cp env/project.env.template env/project.env`
   - Edit `project.env` values.
2. *Environment configuration*
   - `make host-env-prepare`
   - Edit all `env/{DEPLOY_ENV}/configuration/*.env` values you need.
3. *Services configuration*
   - `make host-apply-config`

### Project first run

1. Initialise and run project infrastructure
   - `make infra-init`
2. Run services
   - `make services-run-all`


## Common usage

### Day to day development

If you want to work, you may need some basic commands :
- `make infra-run` / `infra-stop` / `infra-restart`
- `make services-run-all`

**TODO** : add some generic documentation

### Run tests 

**TODO**

## Thanks and resources

- [Git documentation][git]


[git]: https://git-scm.com/doc "Git documentation"

